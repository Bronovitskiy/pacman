import pacmanUrl from './pac-man.svg';
import Wall from './../wall';
import Coin from './../coin';
import playerParams from './config';

export default class Player {
  constructor(dimension, draw, game, startPosition) {
    this.dimension = dimension;
    this.draw = draw;
    this.image = null;
    this.game = game;
    this.score = 0;
    this.life = playerParams.life;
    this.startPosition = startPosition;
    this.preventDirection = null;
    this.newDirection = null;
    this.animationId = null;
    this.isStopAnimation = false;

    this.move = this.move.bind(this);
    this.checkKey = this.checkKey.bind(this);
    this.changeDirection = this.changeDirection.bind(this);
    this.addListeners();
    this.render();
    this.initLife();
    this.startAnimate();
  }

  render() {
    this.image = this.draw.image(pacmanUrl, this.dimension, this.dimension)
      .move(this.startPosition.x, this.startPosition.y);
  }

  addListeners() {
    document.addEventListener('keydown', this.checkKey);
  }

  checkKey(e) {
    const event = e || window.event;
    if (event.key === playerParams.keyUp) {
      this.changeDirection(playerParams.directionTop);
    } else if (event.key === playerParams.keyDown) {
      this.changeDirection(playerParams.directionBottom);
    } else if (event.key === playerParams.keyLeft) {
      this.changeDirection(playerParams.directionLeft);
    } else if (event.key === playerParams.keyRight) {
      this.changeDirection(playerParams.directionRight);
    }
  }

  moveTo(x, y) {
    const coordinate = this.getCoordinate(x, y);
    if (!Wall.checkEntrance(this.game.walls, coordinate)) {
      Coin.checkEntrance(coordinate, this.game, this);
      this.image.move(x, y);
      return true;
    }
    return false;
  }

  startAnimate() {
    this.animationId = requestAnimationFrame(this.move);
  }

  move() {
    if (this.isStopAnimation) {
      this.clearPlayer();
      return;
    }
    if (this.checkDirection(this.newDirection)) {
      this.preventDirection = null;
    } else {
      this.checkDirection(this.preventDirection);
    }
    requestAnimationFrame(this.move);
  }

  checkDirection(direction) {
    let isMove = false;
    if (direction === playerParams.directionTop) {
      isMove = this.moveTo(this.image.x(), this.image.y() - playerParams.boost);
    } else if (direction === playerParams.directionBottom) {
      isMove = this.moveTo(this.image.x(), this.image.y() + playerParams.boost);
    } else if (direction === playerParams.directionLeft) {
      isMove = this.moveTo(this.image.x() - playerParams.boost, this.image.y());
    } else if (direction === playerParams.directionRight) {
      isMove = this.moveTo(this.image.x() + playerParams.boost, this.image.y());
    }
    return isMove;
  }

  getCoordinate(x = this.image.x(), y = this.image.y()) {
    return {
      bottom: y + this.image.height(),
      top: y,
      left: x,
      right: x + this.image.width(),
    };
  }

  updateScore(addScore) {
    const htmlScore = document.querySelector(playerParams.scoreClass);
    this.score += addScore;
    htmlScore.innerHTML = this.score;
  }

  initLife() {
    const htmlLife = document.querySelector(playerParams.lifeClass);
    htmlLife.innerHTML = this.life;
  }

  loseLife() {
    const htmlLife = document.querySelector(playerParams.lifeClass);
    this.life -= 1;
    this.preventDirection = null;
    this.newDirection = null;
    this.image.move(this.startPosition.x, this.startPosition.y);
    htmlLife.innerHTML = this.life;
    if (this.life === 0) {
      this.isStopAnimation = true;
    }
  }

  changeDirection(newDirection) {
    if (newDirection !== this.newDirection) {
      this.preventDirection = this.newDirection;
      this.newDirection = newDirection;
    }
  }

  clearPlayer() {
    cancelAnimationFrame(this.animationId);
    this.image.remove();
    this.player = null;
  }
}
