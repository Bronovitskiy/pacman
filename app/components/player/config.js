const playerParams = {
  boost: 1,
  life: 3,
  keyUp: 'ArrowUp',
  keyDown: 'ArrowDown',
  keyLeft: 'ArrowLeft',
  keyRight: 'ArrowRight',
  directionTop: 'top',
  directionBottom: 'bottom',
  directionLeft: 'left',
  directionRight: 'right',
  lifeClass: '.life',
  scoreClass: '.score',
};

export default playerParams;
