import Field from './../field';

export default class Wall {
  static coordinate(wall) {
    return {
      bottom: wall.y() + wall.height(),
      top: wall.y(),
      left: wall.x(),
      right: wall.x() + wall.width(),
    };
  }

  static checkEntrance(walls, player) {
    let intoWall = false;
    walls.forEach((item) => {
      const wall = Wall.coordinate(item);
      if (intoWall === false) {
        intoWall = Field.entrance(player, wall);
      }
    });
    return intoWall;
  }
}
