import SVG from 'svg.js/dist/svg.min';
import fieldParams from './config';

export default class Field {
  static entrance(objF, objS) {
    return (this.entranceFrom(fieldParams.entranceFromTop, objF, objS)
      || this.entranceFrom(fieldParams.entranceFromBottom, objF, objS))
      && (this.entranceFrom(fieldParams.entranceFromLeft, objF, objS)
      || this.entranceFrom(fieldParams.entranceFromRight, objF, objS));
  }

  static entranceFrom(side, objF, objS) {
    let entrance = null;
    switch (side) {
      case fieldParams.entranceFromTop:
      {
        entrance = objF.top < objS.bottom && objF.top > objS.top;
        break;
      }
      case fieldParams.entranceFromBottom:
      {
        entrance = objF.bottom > objS.top && objF.bottom < objS.bottom;
        break;
      }
      case fieldParams.entranceFromLeft:
      {
        entrance = objF.left < objS.right && objF.left > objS.left;
        break;
      }
      case fieldParams.entranceFromRight:
      {
        entrance = objF.right > objS.left && objF.right < objS.right;
        break;
      }
      default:
    }

    return entrance;
  }


  constructor() {
    this.draw = null;
    this.render();
  }

  render() {
    this.draw = SVG(fieldParams.idName).size(fieldParams.width, fieldParams.height);
    this.draw.rect(this.width, this.height).fill(fieldParams.backgroundColor);
  }
}
