const fieldParams = {
  backgroundColor: '#E3E8E6',
  width: 460,
  height: 310,
  idName: 'pac-man',
  entranceFromTop: 'Top',
  entranceFromBottom: 'Bottom',
  entranceFromLeft: 'Left',
  entranceFromRight: 'Right',
};

export default fieldParams;

