import Field from './../field';
import Player from './../player';
import Enemy from './../enemy';
import { mapArray } from './levels';
import { wallParams, coinParams, idItemParams } from './config';

export default class Game {
  constructor() {
    this.walls = [];
    this.coins = [];
    this.enemies = [];
    this.player = null;
    this.field = null;
    this.elemHeight = null;
    this.elemWidth = null;
  }

  start() {
    this.field = new Field();

    this.elemHeight = Math.round(this.field.draw.height() / mapArray.length);
    this.elemWidth = Math.round(this.field.draw.width() / mapArray[0].length);

    mapArray.forEach((line, row) => {
      line.split('').forEach((item, column) => {
        const startPosition = { x: column * this.elemWidth, y: row * this.elemHeight };
        switch (item) {
          case idItemParams.wall: {
            this.createWall(startPosition);
            break;
          }
          case idItemParams.coin:
          {
            this.createCoin(startPosition);
            break;
          }
          case idItemParams.enemy:
          {
            this.createEnemy(startPosition, row, column);
            break;
          }
          case idItemParams.player:
          {
            this.createPlayer(startPosition);
            break;
          }
          default:
        }
      });
    });
  }

  createWall(startPosition) {
    const wall = this.field.draw
      .rect(this.elemWidth, this.elemHeight).fill(wallParams.backgroundColor)
      .move(startPosition.x, startPosition.y);
    this.walls.push(wall);
  }

  createCoin(startPosition) {
    const width = Math.min(this.elemWidth, this.elemHeight);
    const coin = this.field.draw.circle(width).fill(coinParams.backgroundColor)
      .move(startPosition.x, startPosition.y).radius(coinParams.radius);
    this.coins.push(coin);
  }

  createEnemy(startPosition, row, column) {
    const enemy = new Enemy(this, startPosition, row, column);
    this.enemies.push(enemy);
  }

  createPlayer(startPosition) {
    const width = Math.min(this.elemWidth, this.elemHeight) - 0.1;
    this.player = new Player(width, this.field.draw, this, startPosition);
  }
}
