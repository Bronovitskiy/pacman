import Field from './../field';

export default class Coin {
  static coordinate(coin) {
    return {
      bottom: coin.y(),
      top: coin.y(),
      left: coin.x(),
      right: coin.x(),
    };
  }

  static checkEntrance(playerCoordinate, field, player) {
    let intoCoin = false;

    field.coins.forEach((item, index) => {
      const coin = Coin.coordinate(item);

      if (intoCoin === false) {
        intoCoin = Field.entrance(coin, playerCoordinate);
        if (intoCoin) {
          item.remove();
          field.coins.splice(index, 1);
          player.updateScore(20);
        }
      }
    });

    if (field.coins.length === 0) {
      console.log('Победа');
    }
    return intoCoin;
  }
}
