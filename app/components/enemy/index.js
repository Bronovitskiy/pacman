import ghostUrl from './ghost.svg';
import Wall from './../wall';
import Field from './../field';
import enemyParams from './config';
import { mapArray } from './../game/levels';

export default class Enemy {
  constructor(game, startPosition, row, column) {
    this.width = game.elemWidth - 1;
    this.height = game.elemHeight - 1;
    this.image = null;
    this.game = game;
    this.direction = null;
    this.boost = enemyParams.boost;

    this.randomMove = this.randomMove.bind(this);
    this.render(startPosition);
    this.defaultDirection(row, column);
    this.startAnimate();
  }

  render({ x, y }) {
    this.image = this.game.field.draw.image(ghostUrl, this.width, this.height)
      .move(x, y);
  }

  defaultDirection(row, column) {
    if (mapArray[row - 1][column] !== enemyParams.wallId
      || mapArray[row + 1][column] !== enemyParams.wallId) {
      this.direction = enemyParams.directionVertical;
    } else {
      this.direction = enemyParams.directionHorizontal;
    }
  }

  startAnimate() {
    requestAnimationFrame(this.randomMove);
  }

  randomMove() {
    const x = this.image.x();
    const y = this.image.y();

    if (this.direction === enemyParams.directionVertical && this.checkPass(x, y)) {
      this.image.move(x, y + this.boost);
    } else if (this.direction === enemyParams.directionHorizontal && this.checkPass(x, y)) {
      this.image.move(x + this.boost, y);
    } else {
      this.boost *= -1;
    }

    this.checkPlayer(x, y);
    requestAnimationFrame(this.randomMove);
  }

  checkPlayer(x, y) {
    const coordinate = this.getCoordinate(x, y);
    if (Field.entrance(coordinate, this.game.player.getCoordinate())) {
      this.game.player.loseLife();
    }
  }

  checkPass(x, y) {
    let coordinate = null;

    if (this.direction === enemyParams.directionVertical) {
      coordinate = this.getCoordinate(x, y + this.boost);
    } else if (this.direction === enemyParams.directionHorizontal) {
      coordinate = this.getCoordinate(x + this.boost, y);
    }

    return !Wall.checkEntrance(this.game.walls, coordinate);
  }

  getCoordinate(x = this.image.x(), y = this.image.y()) {
    return {
      bottom: y + this.image.height(),
      top: y,
      left: x,
      right: x + this.image.width(),
    };
  }
}
