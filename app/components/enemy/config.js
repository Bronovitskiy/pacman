const enemyParams = {
  directionVertical: 'vertical',
  directionHorizontal: 'horizontal',
  wallId: '1',
  boost: 1,
};

export default enemyParams;

